# CSS hacks or conditional comments
CSS hacks or conditional comments, for all IE including IE10, IE11 and all versions of EDGE

ALL IE VERSIONS
```css
			<!--[if IE]>
				<link rel="stylesheet" type="text/css" href="css/estilos2.css" />
			<![endif]-->
```
ALL NON-IE VERSIONS
```css
			<!--[if !IE]><!-->
				<link rel="stylesheet" type="text/css" href="css/estilos1.css" />
			<!--<![endif]-->
```		
IE 10 without hacks, using USER-AGENT sniffing
```css
			html[data-useragent*='MSIE 10.0'] p {
			  color: green;
			}
```			
IE 11 without hacks, using UE sniffing
```css
			html[data-useragent*='rv:11.0'] p {
			  color: pink;
			}
```
ALL Edge versions using @ supports -ms-ime-align
```css
			@ supports (-ms-ime-align:auto) p {
				color:yellow; 
			}
```

Without using USER AGENT sniffing for IE10, IE11
```css
			@media all and (-ms-high-contrast: none), (-ms-high-contrast: active){
				p{
					color: blue;
				}
			}
```